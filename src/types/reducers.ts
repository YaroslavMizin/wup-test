import { newTaskReducer } from "../reducers/taskReducer/taskReducer";

type reducer = ReturnType<typeof newTaskReducer>;