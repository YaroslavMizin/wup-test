export const mock = [
    {
        title: 'Первая',
        details: '12321321',
        date: '12-03-2022',
        file: undefined,
        fulfilled: false,
        outdated: false
    },
    {
        title: 'Вторая',
        details: '32121',
        date: '12-03-2022',
        file: undefined,
        fulfilled: true,
        outdated: false
    },
    {
        title: 'Третья',
        details: '32121',
        date: '12-03-2022',
        file: undefined,
        fulfilled: false,
        outdated: true,
    },
    {
        title: 'Четвертая',
        details: '32121',
        date: '12-03-2022',
        file: undefined,
        fulfilled: true,
        outdated: true,
    }
]

